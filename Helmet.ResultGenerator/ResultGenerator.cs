﻿using System;
using System.Collections.Generic;
using System.Linq;
using Helmet.Data.Common.DTO;
using Helmet.Data.Common.Interface;
using Helmet.ResultGenerator.Interface;

namespace Helmet.ResultGenerator
{
    public class ResultGenerator : IResultGenerator
    {
        private readonly IResultGeneratorRepo repository;

        private List<DivisionDTO> divisions;
        private List<SeasonDTO> regularSeasons; 

        public ResultGenerator(IResultGeneratorRepo repository)
        {
            this.repository = repository;
        }

        public void GenerateResultsForRegularSeasons(short startYear, short endYear)
        {
            repository.CreateRegularSeasons(startYear, endYear);
            divisions = repository.GetAllDivisions();
            regularSeasons = repository.GetRegularSeasons(startYear, endYear);

            foreach (var season in regularSeasons)
            {
                CreateDivisionalGames(season);
            }
        }

        private void CreateDivisionalGames(SeasonDTO season)
        {
            foreach (var divisionDTO in divisions)
            {
                CreateDivisionalGames(season, divisionDTO);
            }
        }

        private void CreateDivisionalGames(SeasonDTO season, DivisionDTO divisionDTO)
        {
            var gamesToInsert = new List<GameDTO>();
            foreach (var teamDTO in divisionDTO.Teams.OrderBy(t => t.TeamId))
            {
                var actualTeam = teamDTO;
                var otherTeams = divisionDTO.Teams.Where(t => t.TeamId > actualTeam.TeamId);
                foreach (var otherTeam in otherTeams)
                {
                    var firstGame = CreateNewGame(season, actualTeam, otherTeam);
                    var secondGame = CreateNewGame(season, otherTeam, actualTeam);
                    gamesToInsert.Add(firstGame);
                    gamesToInsert.Add(secondGame);
                }
            }
            if (gamesToInsert.Any())
            {
                repository.InsertGames(gamesToInsert);
            }
        }

        private GameDTO CreateNewGame(SeasonDTO season, TeamDTO awayTeam, TeamDTO homeTeam)
        {
            var gameDTO = new GameDTO
            {
                SeasonId = season.SeasonId,
                GameDate = new DateTime(2010, 1, 1),
                HomeTeamId = homeTeam.TeamId,
                AwayTeamId = awayTeam.TeamId,
                State = 0
            };
            return gameDTO;
        }
    }
}