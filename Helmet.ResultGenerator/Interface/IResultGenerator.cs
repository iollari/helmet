﻿namespace Helmet.ResultGenerator.Interface
{
    public interface IResultGenerator
    {
        void GenerateResultsForRegularSeasons(short startYear, short endYear);
    }
}
