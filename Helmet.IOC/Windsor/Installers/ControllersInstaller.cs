﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Helmet.IOC.Windsor.Installers
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //container.Register(Classes.FromAssemblyNamed("Helmet.Web.Statistics")
            //    .BasedOn<IController>()
            //    .LifestyleTransient());

            container.Register(Classes.FromAssemblyNamed("Helmet.Web.Admin")
                .BasedOn<IController>()
                .LifestyleTransient());
        }
    }
}
