﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Helmet.Data.Common.Interface;
using Helmet.Data.EF.Repository;
using Helmet.Data.Linq2Sql.Repository;
using Helmet.Facade;
using Helmet.Facade.Interface;
using Helmet.ResultGenerator.Interface;

namespace Helmet.IOC.Windsor.Installers
{
    public class ResultGeneratorInstallers : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For<IResultGeneratorRepo>()
                .ImplementedBy(typeof(ResultGeneratorLinq2SqlRepo))
                .LifestylePerWebRequest());
            //container.Register(Component
            //    .For<IResultGeneratorRepo>()
            //    .ImplementedBy(typeof(ResultGeneratorEFRepo))
            //    .LifestylePerWebRequest());
            container.Register(Component
                .For<IResultGenerator>()
                .ImplementedBy(typeof(ResultGenerator.ResultGenerator))
                .LifestylePerWebRequest());
            container.Register(Component
                .For<IHelmetAdminFacade>()
                .ImplementedBy(typeof(HelmetAdminFacade))
                .LifestylePerWebRequest());
        }
    }
}
