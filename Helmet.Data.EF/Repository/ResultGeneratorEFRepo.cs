﻿using System.Collections.Generic;
using System.Linq;
using Helmet.Data.Common.DTO;
using Helmet.Data.Common.Interface;
using Helmet.Data.EF.Converter;
using Helmet.Data.EF.DataModel.EF;

namespace Helmet.Data.EF.Repository
{
    public class ResultGeneratorEFRepo : IResultGeneratorRepo
    {
        public void InsertGames(List<GameDTO> gameDTOs)
        {
            using (var ctx = new HelmetEntities())
            {
                var gamesToInsert = gameDTOs.ConvertAll(DTOConverters.ConvertDTOToGameEF());
                ctx.Games.AddRange(gamesToInsert);
                ctx.SaveChanges();
            }
        }

        public List<DivisionDTO> GetAllDivisions()
        {
            var result = new List<DivisionDTO>();
            using (var ctx = new HelmetEntities())
            {
                result = ctx.Divisions.ToList().ConvertAll(DTOConverters.ConvertDivisionEFToDTO());
            }
            return result;
        }

        public void CreateRegularSeasons(short startYear, short endYear)
        {
            using (var ctx = new HelmetEntities())
            {
                var seasonsToAdd = new List<Season>();
                for (var actualYear = startYear; actualYear <= endYear; actualYear++)
                {
                    seasonsToAdd.Add(new Season
                    {
                        Year = actualYear,
                        PostSeason = false,
                        PreSeason = false
                    });
                }
                if (seasonsToAdd.Any())
                {
                    ctx.Seasons.AddRange(seasonsToAdd);
                    ctx.SaveChanges();
                }
            }
        }

        public List<SeasonDTO> GetRegularSeasons(short startYear, short endYear)
        {
            var result = new List<SeasonDTO>();
            using (var ctx = new HelmetEntities())
            {
                result = ctx.Seasons
                    .Where(s => (s.Year >= startYear) && (s.Year <= endYear))
                    .ToList().ConvertAll(DTOConverters.ConvertSeasonEFToDTO());
            }
            return result;
        }
    }
}