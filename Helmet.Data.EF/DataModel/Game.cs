//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Helmet.Data.EF.DataModel.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Game
    {
        public Game()
        {
            this.GameDetails = new HashSet<GameDetail>();
        }
    
        public int GameId { get; set; }
        public System.DateTime GameDate { get; set; }
        public int HomeTeamId { get; set; }
        public Nullable<short> HomeScore { get; set; }
        public int AwayTeamId { get; set; }
        public Nullable<short> AwayScore { get; set; }
        public bool OT { get; set; }
        public int SeasonId { get; set; }
        public Nullable<byte> State { get; set; }
        public Nullable<byte> Week { get; set; }
    
        public virtual Team Team { get; set; }
        public virtual Team Team1 { get; set; }
        public virtual Season Season { get; set; }
        public virtual ICollection<GameDetail> GameDetails { get; set; }
    }
}
