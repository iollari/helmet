﻿using System;
using System.Linq;
using Helmet.Data.Common.DTO;
using Helmet.Data.EF.DataModel.EF;

namespace Helmet.Data.EF.Converter
{
    public class DTOConverters
    {
        public static Converter<Division, DivisionDTO> ConvertDivisionEFToDTO()
        {
            var converter = new Converter<Division, DivisionDTO>(input =>
            {
                var divisionDTO = new DivisionDTO
                {
                    DivisionId = input.DivisionId,
                    Conference = input.Conference,
                    Name = input.Name,
                    Teams = input.Teams.ToList().ConvertAll(ConvertTeamEFToDTO())
                };
                return divisionDTO;
            });
            return converter;
        }

        private static Converter<Team, TeamDTO> ConvertTeamEFToDTO()
        {
            var converter = new Converter<Team, TeamDTO>(input =>
            {
                var teamDTO = new TeamDTO
                {
                    TeamId = input.TeamId,
                    Code = input.Code,
                    Name = input.Name,
                    DivisionId = input.Division.DivisionId
                };
                return teamDTO;
            });
            return converter;
        }

        public static Converter<Season, SeasonDTO> ConvertSeasonEFToDTO()
        {
            var converter = new Converter<Season, SeasonDTO>(input =>
            {
                var seasonDTO = new SeasonDTO
                {
                    SeasonId = input.SeasonId,
                    Year = input.Year,
                    PostSeason = input.PostSeason,
                    PreSeason = input.PreSeason,
                    //Games = input.Games.ToList().ConvertAll(ConvertGameToDTO())
                };
                return seasonDTO;
            });
            return converter;
        }

        public static Converter<GameDTO, Game> ConvertDTOToGameEF()
        {
            var converter = new Converter<GameDTO, Game>(input =>
            {
                var game = new Game
                {
                    GameId = input.GameId,
                    GameDate = input.GameDate,
                    SeasonId = input.SeasonId,
                    HomeTeamId = input.HomeTeamId,
                    HomeScore = input.HomeScore,
                    AwayTeamId = input.AwayTeamId,
                    AwayScore = input.AwayScore,
                    OT = input.OT,
                    State = input.State,
                    Week = input.Week
                };
                return game;
            });
            return converter;
        }
    }
}
