﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Helmet.Facade.Interface;

namespace Helmet.Web.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHelmetAdminFacade facade;

        public HomeController(IHelmetAdminFacade facade)
        {
            this.facade = facade;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Generate(short startYear, short endYear)
        {
            facade.GenerateResultsForRegularSeasons(startYear, endYear);
            return RedirectToAction("Index");
        }
    }
}
