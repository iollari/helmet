using System.Collections.Generic;
using System.Linq;
using Helmet.Data.Common.DTO;
using Helmet.Data.Common.Interface;
using Helmet.Data.Linq2Sql.Converter;
using Helmet.Data.Linq2Sql.DataModel;

namespace Helmet.Data.Linq2Sql.Repository
{
    public class ResultGeneratorLinq2SqlRepo : IResultGeneratorRepo
    {
        public void InsertGames(List<GameDTO> gameDTOs)
        {
            using (var ctx = new HelmetDataContext())
            {
                var gamesToInsert = gameDTOs.ConvertAll(DTOConverters.ConvertDTOToGame());
                ctx.Games.InsertAllOnSubmit(gamesToInsert);
                ctx.SubmitChanges();
            }            
        }

        public List<DivisionDTO> GetAllDivisions()
        {
            var result = new List<DivisionDTO>();
            using (var ctx = new HelmetDataContext())
            {
                result = ctx.Divisions.ToList().ConvertAll(DTOConverters.ConvertDivisionToDTO());
            }
            return result;
        }

        public void CreateRegularSeasons(short startYear, short endYear)
        {
            using (var ctx = new HelmetDataContext())
            {
                var seasonsToAdd = new List<Season>();
                for (var actualYear = startYear; actualYear <= endYear; actualYear++)
                {
                    seasonsToAdd.Add(new Season
                    {
                        Year = actualYear,
                        PostSeason = false,
                        PreSeason = false
                    });
                }
                if (seasonsToAdd.Any())
                {
                    ctx.Seasons.InsertAllOnSubmit(seasonsToAdd);
                    ctx.SubmitChanges();
                }                
            }
        }

        public List<SeasonDTO> GetRegularSeasons(short startYear, short endYear)
        {
            var result = new List<SeasonDTO>();
            using (var ctx = new HelmetDataContext())
            {
                result = ctx.Seasons
                    .Where(s => (s.Year >= startYear) && (s.Year <= endYear))
                    .ToList().ConvertAll(DTOConverters.ConvertSeasonToDTO());
            }
            return result;
        }
    }
}