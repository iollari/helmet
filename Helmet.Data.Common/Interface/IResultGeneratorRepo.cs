﻿using System.Collections.Generic;
using Helmet.Data.Common.DTO;

namespace Helmet.Data.Common.Interface
{
    public interface IResultGeneratorRepo
    {
        void InsertGames(List<GameDTO> gameDTOs);
        List<DivisionDTO> GetAllDivisions();
        void CreateRegularSeasons(short startYear, short endYear);
        List<SeasonDTO> GetRegularSeasons(short startYear, short endYear);
    }
}
