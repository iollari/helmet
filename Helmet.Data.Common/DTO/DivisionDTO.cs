﻿using System.Collections.Generic;

namespace Helmet.Data.Common.DTO
{
    public class DivisionDTO
    {
        public int DivisionId { get; set; }
        public string Conference { get; set; }
        public string Name { get; set; }
        public List<TeamDTO> Teams { get; set; } 
    }
}
