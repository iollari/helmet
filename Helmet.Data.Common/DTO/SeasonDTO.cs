﻿using System.Collections.Generic;

namespace Helmet.Data.Common.DTO
{
    public class SeasonDTO
    {
        public int SeasonId { get; set; }
        public short Year { get; set; }
        public bool PostSeason { get; set; }
        public bool PreSeason { get; set; }
        public List<GameDTO> Games { get; set; } 
    }
}
