﻿using System;

namespace Helmet.Data.Common.DTO
{
    public class GameDTO
    {
        public int GameId { get; set; }
        public DateTime GameDate { get; set; }
        public int HomeTeamId { get; set; }
        public short? HomeScore { get; set; }
        public int AwayTeamId { get; set; }
        public short? AwayScore { get; set; }
        public bool OT { get; set; }
        public int SeasonId { get; set; }
        public byte? State { get; set; }
        public byte? Week { get; set; }
    }
}
