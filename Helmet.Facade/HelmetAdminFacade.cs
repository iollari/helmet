﻿using Helmet.Facade.Interface;
using Helmet.ResultGenerator.Interface;

namespace Helmet.Facade
{
    public class HelmetAdminFacade : IHelmetAdminFacade
    {
        private readonly IResultGenerator resultGenerator;

        public HelmetAdminFacade(IResultGenerator resultGenerator)
        {
            this.resultGenerator = resultGenerator;
        }

        public void GenerateResultsForRegularSeasons(short startYear, short endYear)
        {
            resultGenerator.GenerateResultsForRegularSeasons(startYear, endYear);
        }
    }
}
