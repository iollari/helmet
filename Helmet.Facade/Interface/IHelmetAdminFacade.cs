﻿namespace Helmet.Facade.Interface
{
    public interface IHelmetAdminFacade
    {
        void GenerateResultsForRegularSeasons(short startYear, short endYear);
    }
}
