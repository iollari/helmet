﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using DapperExtensions;
using Helmet.Data.Common.DTO;
using Helmet.Data.Common.Interface;
using Helmet.Data.Dapper.Converter;
using Helmet.Data.Dapper.DataModel;

namespace Helmet.Data.Dapper.Repository
{
    public class ResultGeneratorDapperRepo : IResultGeneratorRepo
    {
        private readonly string connectionString;            

        public ResultGeneratorDapperRepo()
        {
            // valami 1
            connectionString = ConfigurationManager.ConnectionStrings["Helmet.DapperConnection"].ConnectionString;
        }
        
        public void InsertGames(List<GameDTO> gameDTOs)
        {
            var gamesToInsert = gameDTOs.ConvertAll(DTOConverters.ConvertDTOToGame());
            using (var sqlConnection = new SqlConnection(connectionString))
            {                
                sqlConnection.Open();
                sqlConnection.Insert<Game>(gamesToInsert);
                sqlConnection.Close();
            }
        }

        public List<DivisionDTO> GetAllDivisions()
        {
            var result = new List<DivisionDTO>();
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                var divisions = sqlConnection.GetList<Division>().ToList();
                result = divisions.ConvertAll(DTOConverters.ConvertDivisionToDTO());
                sqlConnection.Close();
            }
            return result;
        }

        public void CreateRegularSeasons(short startYear, short endYear)
        {
            var seasonsToAdd = new List<Season>();
            for (var actualYear = startYear; actualYear <= endYear; actualYear++)
            {
                seasonsToAdd.Add(new Season
                {
                    Year = actualYear,
                    PostSeason = false,
                    PreSeason = false
                });
            }
            if (seasonsToAdd.Any())
            {
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    sqlConnection.Insert<Season>(seasonsToAdd);
                    sqlConnection.Close();
                }
            }
        }

        public List<SeasonDTO> GetRegularSeasons(short startYear, short endYear)
        {
            var result = new List<SeasonDTO>();
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                var predicateGroup = new PredicateGroup {Operator = GroupOperator.And, Predicates = new List<IPredicate>()};
                predicateGroup.Predicates.Add(Predicates.Field<Season>(s => s.Year, Operator.Ge, startYear));
                predicateGroup.Predicates.Add(Predicates.Field<Season>(s => s.Year, Operator.Le, endYear));

                var regularSeasons = sqlConnection.GetList<Season>(predicateGroup).ToList();
                result = regularSeasons.ConvertAll(DTOConverters.ConvertSeasonToDTO());
                sqlConnection.Close();
            }
            return result;
        }
    }
}
